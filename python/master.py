########## PYTHON SCRIPT BEGIN ##########

print ("``Beginning script execution...", end=" ")

# Import scripts
from helpers import form_data, response

# Load json data
form_data.init()

# Prepare response
response.add("input", form_data.all())
response.add("status", "success")
print("initialized.\n")

########## CALCULATION BEGIN ##########

import example_calculation
example_calculation.calculate()

########## CALCULATION END ##########

# Save data
output_file_name = "outputs/" + form_data.id() + "_response.json"
with open(output_file_name, "w") as f:
    f.write(response.get())

print ("\n``Finished execution!")

########## PYTHON SCRIPT END ##########