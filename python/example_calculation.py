from helpers import form_data, response

def calculate ():
    # Store data into individual variables
    planet_template = form_data.get("planet_template", type="str")
    surface_gravity = form_data.get("surface_gravity", type="float")
    planet_radius   = form_data.get("planet_radius",   type="float")

    # Run calculations...
    import time
    time.sleep(3)
    print("``This is some verbose information.")

    # Output example plot results
    import shutil
    example_plot_file = "examples/python/atmos-plot-data.pt"
    output_file_name = "outputs/" + form_data.id() + "_vmr_plotdata.pt"
    shutil.copy(example_plot_file, output_file_name)
    response.add("vmr_file", output_file_name)
    response.add("tp_file", output_file_name)