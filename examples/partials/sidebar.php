<div class="navbar-collapse sidebar-navbar-collapse">
    <ul class="nav navbar-nav" style="float: none;display: block;">
        <li>
            <a href="https://science.gsfc.nasa.gov/sed/index.cfm?fuseAction=people.staffList&navOrgCode=690" data-toggle="dropdown" data-target="#sidebar-dropdown__1">
                People &amp; Organizations
                <div class="pull-right"><span class="caret pull-right"></span></div>
            </a>
            <div class="collapse" id="sidebar-dropdown__1">
                <ul class="nav nav-list">
                    <li><a href="https://science.gsfc.nasa.gov/sed/index.cfm?fuseAction=people.staffList&navOrgCode=690">Staff List</a></li>
                    <li><a href="https://science.gsfc.nasa.gov/sed/index.cfm?fuseAction=people.mgmt&navOrgCode=690">Management Staff</a></li>
                    <li><a href="people/org/chart">Org Chart</a></li>
                    <li><a href="https://science.gsfc.nasa.gov/sed/index.cfm?fuseAction=people.orgList&navOrgCode=690">Org List</a></li>
                </ul>
            </div>
        </li>
        <li>
            <!--<a href="https://science.gsfc.nasa.gov/sed/index.cfm?fuseAction=projects.main&navOrgCode=690" data-toggle="dropdown" data-target="#sidebar-dropdown__2">-->
            <a href="projects/featured" data-toggle="dropdown" data-target="#sidebar-dropdown__2">
                Missions &amp; Projects
                <div class="pull-right"><span class="caret pull-right"></span></div>
            </a>
            <div class="collapse" id="sidebar-dropdown__2">
                <ul class="nav nav-list">
                    <li><a href="projects/featured">Featured</a></li>
                    <li><a href="projects/alphabetical">Full Alphabetical List</a></li>
                    <!--<li><a href="https://science.gsfc.nasa.gov/sed/index.cfm?fuseAction=projects.featured&navOrgCode=690">Featured</a></li>-->
                    <!--<li><a href="https://science.gsfc.nasa.gov/sed/index.cfm?fuseAction=projects.alphabetical&navOrgCode=690">Full Alphabetical List</a></li>-->
                </ul>
            </div>
        </li>
        <li><a href="https://science.gsfc.nasa.gov/sed/index.cfm?fuseAction=publications.main&navOrgCode=690">Publications</a></li>
        <li><a href="https://science.gsfc.nasa.gov/sed/index.cfm?fuseAction=featured.main">Today's Science</a></li>
        <li>
            <a href="https://science.gsfc.nasa.gov/sed/index.cfm?fuseAction=highlight.main&navOrgCode=690" data-toggle="dropdown" data-target="#sidebar-dropdown__3">
                Highlights
                <div class="pull-right"><span class="caret pull-right"></span></div>
            </a>
            <div class="collapse" id="sidebar-dropdown__3">
                <ul class="nav nav-list">
                    <li><a href="https://science.gsfc.nasa.gov/sed/index.cfm?fuseAction=highlight_prweb.main&navOrgCode=690">Press Releases & Feature Stories</a></li>
                    <li><a href="https://science.gsfc.nasa.gov/sed/index.cfm?fuseAction=highlight_pres.main&navOrgCode=690">Presentations</a></li>
                    <li><a href="https://science.gsfc.nasa.gov/sed/index.cfm?fuseAction=highlight_report.main&navOrgCode=690">Reports</a></li>
                    <li><a href="https://science.gsfc.nasa.gov/sed/index.cfm?fuseAction=localnews.main&navOrgCode=690">Local News</a></li>
                    <li><a href="https://science.gsfc.nasa.gov/sed/index.cfm?fuseAction=highlight_image.main&navOrgCode=690">Images</a></li>
                    <li><a href="https://science.gsfc.nasa.gov/sed/index.cfm?fuseAction=highlight_arch.ed&navOrgCode=690&filt=true">STEM Resources</a></li>
                    <li><a href="https://science.gsfc.nasa.gov/sed/index.cfm?fuseAction=highlight_arch.ap&navOrgCode=690">Goddard Apps</a></li>
                </ul>
            </div>
        </li>
        <li><a href="https://science.gsfc.nasa.gov/sed/index.cfm?fuseAction=community.main&navOrgCode=690">Calendar</a></li>
        <li>
            <a href="https://science.gsfc.nasa.gov/sed/index.cfm?fuseAction=highlight.main&navOrgCode=690" data-toggle="dropdown" data-target="#sidebar-dropdown__4">
                Awards
                <div class="pull-right"><span class="caret pull-right"></span></div>
            </a>
            <div class="collapse" id="sidebar-dropdown__4">
                <ul class="nav nav-list">
                    <li><a href="https://science.gsfc.nasa.gov/sed/index.cfm?fuseAction=award.AwardsWon&navOrgCode=690">Awards Won</a></li>
                    <li><a href="https://science.gsfc.nasa.gov/sed/index.cfm?fuseAction=award.AwardsNominate&navOrgCode=690">How to Nominate</a></li>
                </ul>
            </div>
        </li>
        <li><a href="https://science.gsfc.nasa.gov/sed/index.cfm?fuseAction=faq.main&navOrgCode=690">FAQ</a></li>
        <li><hr/></li>
        <li><a href="https://science.gsfc.nasa.gov/690/seminars.html">Seminars and Meetings</a></li>
        <li><a href="https://science.gsfc.nasa.gov/690/multimedia.html">Multimedia</a></li>
        <li><a href="https://science.gsfc.nasa.gov/690/photos.html">Photos</a></li>
        <li><a href="https://science.gsfc.nasa.gov/690/solarsystemtour.html">Solar System Tour</a></li>
        <li><hr/></li>
        <li><a href="https://science.gsfc.nasa.gov/sed/index.cfm?fuseAction=comment.main&navOrgCode=690">Comment Form</a></li>
        <li><hr/></li>
        <li class="always-open">
            <a href="#">Quick Links</a>
            <div class="collapse" id="sidebar-dropdown__quick-links">
                <ul class="nav nav-list">
                    <li><a href="http://astrobiology.gsfc.nasa.gov/">Goddard Center for Astrobiology</a></li>
                    <li><a href="http://ssed.gsfc.nasa.gov/dream/">Goddard DREAM Center for Lunar Studies</a></li>
                </ul>
            </div>
        </li>
    </ul>
</div>