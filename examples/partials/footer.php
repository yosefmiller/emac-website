<div class="navbar-brand">
    <a class="navbar-brand_logo" href="https://www.nasa.gov/goddard/">
        <img src="img/goddard_logo.png" alt="Goddard Space Flight Center">
    </a>
</div>
<ul class="nav navbar-nav navbar-right">
    <li>
        <span>NASA Official: </span>
        <script type="text/javascript" language="javascript">
			//<!--
			// Email obfuscator script 2.1 by Tim Williams, University of Arizona
			// Random encryption key feature coded by Andrew Moulden
			// This code is freeware provided these four comment lines remain intact
			// A wizard to generate this code is at http://www.jottings.com/obfuscator/
			{ coded = "d2o.sdhVPZZ@hdgd.lm2";
				key = "K4dgQoOxbt2TY9nqc1AyWuVlSmCMB80z3ehHrjL6XfRZasDiIpENvPUJF7kwG5";
				shift=coded.length;
				link="";
				for (var i=0; i<coded.length; i++) {
					var ltr, link;
					if (key.indexOf(coded.charAt(i))===-1) {
						ltr = coded.charAt(i);
						link += (ltr);
					}
					else {
						ltr = (key.indexOf(coded.charAt(i))-shift+key.length) % key.length;
						link += (key.charAt(ltr));
					}
				}
				document.write("<a href='mailto:"+link+"'>Avi Mandell</a>");
			}
			//-->
        </script>
        <noscript><a href="#">Avi Mandell</a></noscript>
    </li>
    <li>
        <span>Curator: </span>
        <script type="text/javascript" language="javascript">
			//<!--
			// Email obfuscator script 2.1 by Tim Williams, University of Arizona
			// Random encryption key feature coded by Andrew Moulden
			// This code is freeware provided these four comment lines remain intact
			// A wizard to generate this code is at http://www.jottings.com/obfuscator/
			{ coded = "Jos9.A.cEif1ff1s@7oio.6Ez";
				key = "rh3vp7ZO2ImHRTFUlNjA4g1doscMzVxtWqwCSXyKB9nDGQ6bLEiJ50Pkf8Yeua";
				shift=coded.length;
				link="";
				for (i=0; i<coded.length; i++) {
					var ltr, link;
					if (key.indexOf(coded.charAt(i))===-1) {
						ltr = coded.charAt(i);
						link += (ltr);
					}
					else {
						ltr = (key.indexOf(coded.charAt(i))-shift+key.length) % key.length;
						link += (key.charAt(ltr));
					}
				}
				document.write("<a href='mailto:"+link+"'>Carl F. Hostetter</a>");
			}
			//-->
        </script>
        <noscript><a href="#">Carl F. Hostetter</a></noscript>
    </li>
    <li>
        <a href="https://www.nasa.gov/about/highlights/HP_Privacy.html">Privacy and Security Notices</a>
    </li>
</ul>
<div class="nav navbar-nav navbar-right hidden">
    <div class="row">
        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
            <div class="text-muted">
                © 2017 NASA Goddard, All rights reserved
            </div>
        </div>
    </div>
</div>