<!-- MAIN SECTION -->
<div class="col-md-12">
    <div class="content-section">
        <h4 class="content-section__title">Featured Missions & Projects - Solar System Exploration Division (690)</h4>
        <div class="row">
            <div class="col-sm-2 col-xs-8 col-sm-push-0 col-xs-push-2">
                <a href="http://www.nasa.gov/mission_pages/LRO/overview/index.html" style="display:block;padding-bottom:10px;">
                    <img style="width: 100%;" src="https://science.gsfc.nasa.gov/sed/images/mission_thumbs/lro_100.jpg"/>
                </a>
            </div>
            <div class="col-sm-10 col-xs-12">
                <p class="content-section__subtitle">
                    <a href="http://www.nasa.gov/mission_pages/LRO/overview/index.html">Lunar Reconnaissance Orbiter (LRO)</a>
                </p>
                <p>The Lunar Reconnaissance Orbiter (LRO) is an unmanned spacecraft designed to create a comprehensive atlas of the moon's physical features, radiation environment, temperatures, and resources. The mission places special emphasis on the moon's polar regions, where permanently shadowed craters may contain significant amounts of water ice that future human explorers might be able to exploit. LRO launched on June 18, 2009.</p>
            </div>
        </div>
        <div class="row">
            <hr class="col-md-12"/>
            <div class="col-sm-2 col-xs-8 col-sm-push-0 col-xs-push-2">
                <a href="https://science.gsfc.nasa.gov/sed/index.cfm?fuseAction=projects.view&project_id=244" style="display:block;padding-bottom:10px;">
                    <img style="width: 100%;" src="https://science.gsfc.nasa.gov/sed/images/mission_thumbs/LASP_maven_01.jpg"/>
                </a>
            </div>
            <div class="col-sm-10 col-xs-12">
                <p class="content-section__subtitle">
                    <a href="https://science.gsfc.nasa.gov/sed/index.cfm?fuseAction=projects.view&project_id=244">Mars Atmosphere and Volatile Evolution Mission (MAVEN)</a>
                </p>
                <p>The Mars Atmosphere and Volatile Evolution (MAVEN) mission is the first orbiter devoted to understanding the Martian upper atmosphere. Research suggests suggests that Mars once had a thicker atmosphere and was warm enough for liquid water to flow on the surface. MAVEN's instruments are exploring Mars' upper atmosphere, ionosphere, and interactions with the sun and solar wind to determine the role played by loss of atmospheric gas to space in the evolution of the Martian climate through time. Goddard's Planetary Environments Laboratory developed MAVEN's Neutral Gas and Ion Mass Spectrometer and the Planetary Magnetospheres Laboratory developed MAVEN's magnetometer. The MAVEN mission launched November 18, 2013.</p>
                <p><a href="javascript:void(0)" data-toggle="collapse" data-target="#key-staff-dropdown__1"><b>-/+ Key Staff</b></a></p>
                <p class="collapse" id="key-staff-dropdown__1">
                    - Project Scientist: <a href="https://science.gsfc.nasa.gov/sed/index.cfm?fuseAction=people.jumpBio&navOrgCode=690&navTab=nav_about_us&iphonebookid=11822">Joseph M Grebowsky</a>
                </p>
            </div>
        </div>
        <hr class="col-md-12"/>
        <p><a class="content-section__link" href="projects/alphabetical">See Full Alphabetical List</a></p>
    </div>
</div>
<!-- END MAIN SECTION -->