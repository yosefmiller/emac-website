<!-- MAIN SECTION -->
<div class="col-md-8">
    <div class="content-section">
        <h4 class="content-section__title">Solar System Exploration Division (690) Home</h4>
        <a class="story-section img-caption" href="http://www.nasa.gov/feature/goddard/2017/goddard-hosts-8th-annual-science-jamboree-for-employees">
            <div class="img-background" style="background-image: url('https://science.gsfc.nasa.gov/sed/images/highlight_thumbs/HighLightID_7381.jpg')"></div>
            <span>FEATURED STORY<br/><b>Goddard Hosts 8th Annual Science Jamboree for Employees</b></span>
        </a>
        <a class="story-section story-section-split img-caption" href="http://www.nasa.gov/feature/goddard/2017/michael-flasar-a-back-of-the-envelope-scientist-and-cassini-principal-investigator">
            <div class="img-background" style="background-image: url('https://science.gsfc.nasa.gov/sed/images/highlight_thumbs/HighLightID_7315.jpg')"></div>
            <span>FEATURED STORY<br/><b>Michael Flasar - A "Back of the Envelope" Scientist and Cassini Principal Investigator</b></span>
        </a>
        <a class="story-section story-section-split img-caption" href="http://www.nasa.gov/feature/goddard/2017/maven-1000-days">
            <div class="img-background" style="background-image: url('https://science.gsfc.nasa.gov/sed/images/highlight_thumbs/HighLightID_7288.png')"></div>
            <span>FEATURED STORY<br/><b>1,000 Days in Orbit: MAVEN’s Top 10 Discoveries at Mars</b></span>
        </a>
        <p><a class="content-section__link" href="https://science.gsfc.nasa.gov/sed/index.cfm?fuseAction=highlight_prweb.main&navOrgCode=690&navTab=nav_about_us">See all Press Releases & Feature Stories</a></p>
    </div>
    <div class="content-section">
        <h4 class="content-section__title">Division News</h4>
        <p class="content-section__subtitle">More Goddard CubeSat Mission Concepts Selected for Study</p>
        <p class="content-section__date">07.19.2017</p>
        <p>
            Researchers in Code 690 have leadership roles in six studies selected under the Planetary Science Deep Space SmallSat Studies (PSDS3) program to develop mission concepts for small satellites.
            <br/>1. Valeria Cottini, PI: the CubeSat UV Experiment (CUVE);
            <br/>2. Timothy Stubbs, PI: Bi-sat Observations of the Lunar Atmosphere above Swirls (BOLAS);
            <br/>3. Tilak Hewagama, PI: Primitive Object Volatile Explorer (PrOVE);
            <br/>4. Noah Petro, PI: Mini Lunar Volatiles (MiLUV) mission;
            <br/>5. Mike Collier, PI: Phobos Regolith Ion Sample Mission (PRISM);
            <br/>6. Barbara Cohen, co-Investigator: Lunar Water Assessment, Transport, Evolution, and Resource (WATER) mission.
        </p>
        <hr/>
        <p class="content-section__subtitle"><a href="https://science.gsfc.nasa.gov/600/images/scijamboree2017/">Science Jamboree 2017 is in the books</a></p>
        <p class="content-section__date">07.19.2017</p>
        <div class="img-caption">
            <img class="img-thumbnail" src="https://science.gsfc.nasa.gov/600/images/scijamboree2017/SciJam17_40.jpg"/>
            <span>The Science Jamboree was a huge success.<br/><small>Photos are available (click the title of this item).</small></span>
        </div>
        <br/>
        <p><a class="content-section__link" href="https://science.gsfc.nasa.gov/sed/index.cfm?fuseAction=localnews.main&navOrgCode=690">More Division News</a></p>
    </div>
</div>
<!-- END MAIN SECTION -->

<!-- RIGHT SECTION -->
<div class="col-md-4">
    <div class="content-section">
        <h4 class="content-section__title">Overview</h4>
        <p>The <b>Solar System Exploration Division</b> conducts theoretical and experimental research to explore the solar system and understand the formation and evolution of planetary systems. Laboratories within the Division investigate areas as diverse as astrochemistry, planetary atmospheres, extrasolar planetary systems, earth science, planetary geodynamics, space geodesy, and comparative planetary studies.</p>
        <p>To study how planetary systems form and evolve, Division scientists develop theoretical models as well as the investigations and space instruments to test them. The researchers participate in planetary and earth science missions; collect, interpret, and evaluate measurements; and publish conclusions based on this research. The Division archives and disseminates the data, provides expert user support, and offers education and public outreach programs about the Division's science missions and services.</p>
    </div>
    <div class="content-section">
        <h4 class="content-section__title">Contact Us</h4>
        <p><b>Theresa A Wirth</b><br/>
            301.614.6019<br/>
            Administrative Support Specialist [690]<br/>
            <script type="text/javascript" language="javascript">
				<!--
				// Email obfuscator script 2.1 by Tim Williams, University of Arizona
				// Random encryption key feature coded by Andrew Moulden
				// This code is freeware provided these four comment lines remain intact
				// A wizard to generate this code is at http://www.jottings.com/obfuscator/
				{ coded = "L6A4A8d.d.Fn4L6@Sd8d.ypJ";
					key = "XdkoOJi0Uemg58FMVjzKNH4lq29pc1nQtAhyGxuaW3bvTfPDIZEswCSYL76BrR";
					shift=coded.length;
					link="";
					for (var i=0; i<coded.length; i++) {
						var ltr, link;
						if (key.indexOf(coded.charAt(i))===-1) {
							ltr = coded.charAt(i);
							link += (ltr);
						}
						else {
							ltr = (key.indexOf(coded.charAt(i))-shift+key.length) % key.length;
							link += (key.charAt(ltr));
						}
					}
					document.write("<a href='mailto:"+link+"'>"+link+"</a>");
				}
				//-->
            </script>
        </p>
        <p><b>Rita A Melvin</b><br/>
            301.614.6011<br/>
            Administrative Assistant [690]<br/>
            <script type="text/javascript" language="javascript">
				<!--
				// Email obfuscator script 2.1 by Tim Williams, University of Arizona
				// Random encryption key feature coded by Andrew Moulden
				// This code is freeware provided these four comment lines remain intact
				// A wizard to generate this code is at http://www.jottings.com/obfuscator/
				{ coded = "9g8v.v.lRsYgt@tvUv.cEY";
					key = "qLWyvnT3Rm6oJUMXfVj4cO1A02YtQBZlNEKrdhiu7ISzaxpGe8Hw5sDCb9kPgF";
					shift=coded.length;
					link="";
					for (var i=0; i<coded.length; i++) {
						var ltr, link;
						if (key.indexOf(coded.charAt(i))===-1) {
							ltr = coded.charAt(i);
							link += (ltr);
						}
						else {
							ltr = (key.indexOf(coded.charAt(i))-shift+key.length) % key.length;
							link += (key.charAt(ltr));
						}
					}
					document.write("<a href='mailto:"+link+"'>"+link+"</a>");
				}
				//-->
            </script>
        </p>
        <p><b>Leslie A McClare</b><br/>
            301.614.6010<br/>
            Administrative Assistant [690]<br/>
            <script type="text/javascript" language="javascript">
				<!--
				// Email obfuscator script 2.1 by Tim Williams, University of Arizona
				// Random encryption key feature coded by Andrew Moulden
				// This code is freeware provided these four comment lines remain intact
				// A wizard to generate this code is at http://www.jottings.com/obfuscator/
				{ coded = "Sy1S3y.n.z00SnDy@5n1n.rYf";
					key = "s3XcAJVa8wfRLqj7YHlDeWF6z1BU0GTgnNE2ZMiQIubS9yOvkKphtoCPr54dxm";
					shift=coded.length;
					link="";
					for (var i=0; i<coded.length; i++) {
						var ltr, link;
						if (key.indexOf(coded.charAt(i))===-1) {
							ltr = coded.charAt(i);
							link += (ltr);
						}
						else {
							ltr = (key.indexOf(coded.charAt(i))-shift+key.length) % key.length;
							link += (key.charAt(ltr));
						}
					}
					document.write("<a href='mailto:"+link+"'>"+link+"</a>");
				}
				//-->
            </script>
        </p>
    </div>
</div>
<!-- END RIGHT SECTION -->