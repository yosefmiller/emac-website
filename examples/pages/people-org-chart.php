<!-- MAIN SECTION -->
<div class="col-md-12">
    <div class="content-section">
        <h4 class="content-section__title">Sciences and Exploration Directorate (690) Organization Chart</h4>
        <p><a class="content-section__link" href="https://science.gsfc.nasa.gov/sed/content/uploadFiles/orgchart_pdf/690_orgchart.pdf">Download Printable Org Chart</a></p>
        <div class="org-chart">
            <div class="well org-chart-box">
                <div class="org-chart-item-title"><b>SOLAR SYSTEM EXPLORATION DIVISION | Code 690</b></div>
                <hr/>
                <div class="org-chart-item-staff">
                    <div><b>Director:</b> Paul R Mahaffy</div>
                    <div><b>Acting Deputy Director:</b> Stephanie A Getty</div>
                    <div><b>Associate Director for Strategic Science:</b> Daniel P Glavin</div>
                    <div><b>Associate Director for Planning, Research and Development:</b> Brook Lakew</div>
                    <div><b>Assistant Director:</b> Juri Min Schauermann</div>
                    <div><b>Administrative Support Specialist:</b> Theresa A Wirth</div>
                    <div><b>Administrative Assistant:</b> Rita A Melvin</div>
                    <div><b>Administrative Assistant:</b> Leslie A McClare</div>
                </div>
            </div>
            <div class="org-chart-split">
                <div class="label label-accent org-chart-label">Offices</div>
                <ul class="clearfix">
                    <li>
                        <div class="org-chart-box">
                            <div class="well">
                                <div class="org-chart-item-title"><b>Solar System Exploration Data Services Office | Code 690.1</b></div>
                                <hr/>
                                <div class="org-chart-item-staff">
                                    <div><b>Acting Head:</b> Thomas H Morgan</div>
                                    <div><b>Administrative Assistant:</b> Mona R Drexler</div>
                                </div>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
            <div class="org-chart-split">
                <div class="label label-accent org-chart-label">Laboratories</div>
                <ul class="clearfix">
                    <li>
                        <div class="org-chart-box">
                            <div class="well">
                                <div class="org-chart-item-title"><b>Astrochemistry Laboratory | Code 691</b></div>
                                <hr/>
                                <div class="org-chart-item-staff">
                                    <div><b>Chief:</b> Jason P Dworkin</div>
                                    <div><b>Associate Chief:</b> Reggie L. Hudson</div>
                                    <div><b>Administrative Assistant:</b> Michele L Smith</div>
                                </div>
                            </div>
                        </div>
                        <div class="org-chart-child">
                            <div class="org-chart-box">
                                <div class="well">
                                    <div class="org-chart-item-title"><b>Solar System Exploration Grants Support Office | Code 691.1</b></div>
                                    <hr/>
                                    <div class="org-chart-item-staff">
                                        <div><b>Chief:</b> Jason P Dworkin</div>
                                        <div><b>Associate Chief:</b> Reggie L. Hudson</div>
                                        <div><b>Administrative Assistant:</b> Michele L Smith</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="org-chart-box">
                            <div class="well">
                                <div class="org-chart-item-title"><b>Planetary Systems Lab | Code 693</b></div>
                                <hr/>
                                <div class="org-chart-item-staff">
                                    <div><b>Chief:</b> Jason P Dworkin</div>
                                    <div><b>Associate Chief:</b> Reggie L. Hudson</div>
                                    <div><b>Administrative Assistant:</b> Michele L Smith</div>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="org-chart-box">
                            <div class="well">
                                <div class="org-chart-item-title"><b>Planetary Magnetospheres Lab | Code 695</b></div>
                                <hr/>
                                <div class="org-chart-item-staff">
                                    <div><b>Chief:</b> Jason P Dworkin</div>
                                    <div><b>Associate Chief:</b> Reggie L. Hudson</div>
                                    <div><b>Administrative Assistant:</b> Michele L Smith</div>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="org-chart-box">
                            <div class="well">
                                <div class="org-chart-item-title"><b>Planetary Geodynamics Lab | Code 698</b></div>
                                <hr/>
                                <div class="org-chart-item-staff">
                                    <div><b>Chief:</b> Jason P Dworkin</div>
                                    <div><b>Associate Chief:</b> Reggie L. Hudson</div>
                                    <div><b>Administrative Assistant:</b> Michele L Smith</div>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="org-chart-box">
                            <div class="well">
                                <div class="org-chart-item-title"><b>Planetary Environments Laboratory | Code 699</b></div>
                                <hr/>
                                <div class="org-chart-item-staff">
                                    <div><b>Chief:</b> Jason P Dworkin</div>
                                    <div><b>Associate Chief:</b> Reggie L. Hudson</div>
                                    <div><b>Administrative Assistant:</b> Michele L Smith</div>
                                </div>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
<!-- END MAIN SECTION -->

<style>
    /* Main */
    .org-chart {
        position: relative;
    }
    .org-chart:after {
        content: '';
        width: 50%;
        height: 2px;                            /* @line-width */
        position: absolute;
        left: 25%;
        background: #ccc;
        background: linear-gradient(to right, transparent 0%, #ccc 50%, transparent 100%);
    }
    .org-chart ul {
        list-style: none;
        padding: 0;
        margin: 0;
    }
    .org-chart-box {
        width: 50%;
        margin: 0 auto;
    }
    .org-chart-box hr {
        border-top-color: #ddd;
    }
    .org-chart-item-title {
        text-align: center;
    }
    .org-chart-item-staff {
        text-align: center;
    }

    /** For a tree-like style **/
    ul.org-chart-children {
        margin-top: 40px;       /* @vertical-space */
        position: relative;
    }
    ul.org-chart-children:after {
        content: '';
        position: absolute;
        border: 0;
        left: 50%;
        top: -40px;             /* @vertical-space */
        height: 40px;           /* @vertical-space */
        width: 2px;             /* @line-width */
        background-color: #ccc; /* @line-color */
    }

    /* Row with horizontal bar across top */
    ul.org-chart-row {
        position: relative;
    }
    ul.org-chart-row:after {
        content: '';
        position: absolute;
        border: 0;
        top: 0;
        left: calc(50% / 2);                    /* @num-columns */
        width: calc(100% - calc(100% / 2));     /* @num-columns */
        height: 2px;                            /* @line-width */
        background-color: #ccc;                 /* @line-color */
    }

    /** Row with items on either side of a central vertical bar **/
    .org-chart-split {
        position: relative;
        white-space: nowrap;
        padding-top: 45px;
    }
    /* Adds vertical dividing line */
    .org-chart-split:after {
        content: '';
        position: absolute;
        left: 50%;
        height: 100%;
        width: 2px;                             /* @line-width */
        background-color: #ccc;                 /* @line-color */
        z-index: -10;
        top: 0;
    }
    /* Items should alternate sides of divider */
    .org-chart-split li {
        width: 50%;
        position: relative;
        display: inline-block;
        vertical-align: top;
        white-space: normal;
        margin-bottom: 20px;
        float: left;
    }
    .org-chart-split li.split-left {
        clear: left;
    }
    .org-chart-split li.split-right {
        clear: none;
        float: right;
    }
    /* Adds horizontal line connecting item to central line */
    .org-chart-split li .org-chart-box {
        width: 100%;
        display: inline-block;
        position: relative;
    }
    .org-chart-split li.split-left > .org-chart-box:after,
    .org-chart-split li.split-right > .org-chart-box:before {
        content: '';
        position: absolute;
        top: 50%;
        height: 2px;                            /* @line-width */
        background-color: #ccc;                 /* @line-color */
        width: 25%;
    }
    /* Items should have a 25% space on the inner side */
    .org-chart-split li > .org-chart-box .well {
        width: 75%;
        display: inline-block;
    }
    .org-chart-split li.split-right > .org-chart-box .well {
        margin-left: 25%;
    }
    /* Child of a split item */
    .org-chart-split li .org-chart-child {
        width: 75%;
        margin-top: 20px;
        position: relative;
    }
    .org-chart-split li .org-chart-child:after {
        content: '';
        position: absolute;
        width: 2px;
        background-color: #ccc;
        height: 40px;
        left: 50%;
        top: -40px;
    }
    .org-chart-split li.split-right .org-chart-child {
        margin-left: 25%;
    }

    /** Actual content **/
    .org-chart-label {
        display: table;
        margin: 0 auto 40px;        /* @vertical-space */
        font-size: 19px;
        padding: 7px 14px 7px;
        font-weight: 100;
    }
</style>

<script>
	$(document).ready(function () {
		$(".org-chart-split > ul > li").each(function () {
			if ( $(this)[0].offsetLeft > 10) {
				$(this).addClass("split-right").removeClass("split-left");
            } else {
				$(this).addClass("split-left").removeClass("split-right");
            }
		});
	});
</script>