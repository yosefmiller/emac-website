<!-- MAIN SECTION -->
<div class="col-md-12">
    <div class="content-section">
        <h4 class="content-section__title">All Missions & Projects - Solar System Exploration Division (690)</h4>
        <fieldset class="well">
            <legend class="label label-large label-fieldset label-accent">Search Filter</legend>
            <div class="col-md-4">
                <div class="form-group">
                    <label for="srchStatus">Status:</label>
                    <select class="form-control" name="srchStatus" id="srchStatus" title="Filter by Status">
                        <option value="">All</option>
                        <option value="dev"> In Development </option>
                        <option value="in_orbit_c"> In-orbit Checkout </option>
                        <option value="current"> Operational </option>
                        <option value="past"> Past </option>
                        <option value="future"> Under Study </option>
                    </select>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label for="srchDiscipline">Discipline:</label>
                    <select class="form-control" name="srchDiscipline" id="srchDiscipline" title="Filter by Discipline">
                        <option value="">All</option>
                        <option value="int"> Interdisciplinary </option>
                        <option value="sol"> Solar System </option>
                    </select>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label for="srchKeyword">Text Search:</label>
                    <input class="form-control" type="text" value="" name="srchKeyword" id="srchKeyword" title="Search by Keyword">
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label for="srchClass">Class:</label>
                    <select name="srchClass" id="srchClass" title="Filter by Class" class="input_width_200 searchFilterSelect">
                        <option value="">All</option>
                        <option value="cc"> Computing Center </option>
                        <option value="coe"> Center of Excellence </option>
                        <option value="data"> Data/Image </option>
                        <option value="dc"> Data Centers </option>
                        <option value="edu"> Educational Initiative </option>
                        <option value="flight"> Flight Project </option>
                        <option value="instrument"> Instrument </option>
                        <option value="models"> Computer Models </option>
                        <option value="new_tech"> New Technology </option>
                        <option value="pi"> PI (ex: field campaign) </option>
                        <option value="research_g"> Research Group </option>
                        <option value="software"> Software </option>
                        <option value="work_g"> Working Group </option>
                    </select>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label for="srchCategory">Science Topic:</label>
                    <select class="form-control" name="srchCategory" id="srchCategory">
                        <option value="">All</option>
                        <option value="1"> Earth Science: Precipitation </option>
                        <option value="2"> Earth Science: Aerosols </option>
                        <option value="3"> Earth Science: Clouds </option>
                        <option value="4"> Earth Science: Surface Properties </option>
                        <option value="5"> Earth Science: Remote Sensing </option>
                        <option value="6"> Earth Science: Solar Radiation </option>
                        <option value="7"> Earth Science: Analysis </option>
                        <option value="8"> Earth Science: Theory &amp; Modeling </option>
                        <option value="57"> Earth Science: Vegetation &amp; Soil </option>
                        <option value="58"> Earth Science: Land Cover/Use </option>
                        <option value="59"> Earth Science: Land Disturbance </option>
                        <option value="60"> Earth Science: Ecosystem function </option>
                        <option value="61"> Earth Science: Seasonal Dynamics </option>
                        <option value="62"> Earth Science: Fire </option>
                        <option value="63"> Earth Science: Atmospheric Chemistry </option>
                        <option value="64"> Earth Science: Atmospheric Dynamics </option>
                        <option value="65"> Earth Science: Atmospheric Measurement </option>
                        <option value="66"> Earth Science: Pollution </option>
                        <option value="67"> Earth Science: Ozone </option>
                        <option value="68"> Earth Science: Ocean Dynamics </option>
                        <option value="69"> Earth Science: Hydrology / Water Cycle </option>
                        <option value="70"> Earth Science: Physical Oceanography </option>
                        <option value="71"> Earth Science: Ocean Circulation </option>
                        <option value="72"> Earth Science: Salinity </option>
                        <option value="73"> Earth Science: Interseasonal Variability </option>
                        <option value="74"> Earth Science: Cryosphere / Earth's Ice Cover </option>
                        <option value="75"> Earth Science: Sea Ice </option>
                        <option value="76"> Earth Science: Glaciers </option>
                        <option value="77"> Earth Science: Mass Balance </option>
                        <option value="78"> Earth Science: Hurricanes </option>
                        <option value="79"> Earth Science: Air-Sea Interaction </option>
                        <option value="80"> Earth Science: Human Dimensions </option>
                        <option value="81"> Earth Science: Climate </option>
                        <option value="82"> Earth Science: Global Warming </option>
                        <option value="83"> Earth Science: Weather </option>
                        <option value="84"> Earth Science: Carbon Cycle </option>
                        <option value="85"> Earth Science: Applications </option>
                        <option value="110"> Earth Science: Technology &amp; Missions </option>
                        <option value="46"> Heliophysics: Solar Interior </option>
                        <option value="47"> Heliophysics: Solar Surface </option>
                        <option value="48"> Heliophysics: Solar Atmosphere </option>
                        <option value="49"> Heliophysics: Solar Cycle </option>
                        <option value="50"> Heliophysics: Solar Active Regions </option>
                        <option value="51"> Heliophysics: Transient Events </option>
                        <option value="52"> Heliophysics: Inner Heliosphere </option>
                        <option value="53"> Heliophysics: Outer Heliosphere </option>
                        <option value="54"> Heliophysics: Earth's Magnetosphere </option>
                        <option value="55"> Heliophysics: Earth's Ionosphere </option>
                        <option value="56"> Heliophysics: Earth's Thermosphere </option>
                        <option value="87"> Heliophysics: Atomic/Nuclear Physics </option>
                        <option value="91"> Heliophysics: Plasma Processes </option>
                        <option value="95"> Heliophysics: Space Weather </option>
                        <option value="97"> Heliophysics: Theory &amp; Modeling </option>
                        <option value="107"> Heliophysics: Analysis </option>
                        <option value="112"> Heliophysics: Technology &amp; Missions </option>
                        <option value="26"> Solar System: Atmospheres </option>
                        <option value="27"> Solar System: Magnetospheres </option>
                        <option value="28"> Solar System: Solar System </option>
                        <option value="29"> Solar System: Astrobiology </option>
                        <option value="30"> Solar System: Astrochemistry </option>
                        <option value="31"> Solar System: Geophysics </option>
                        <option value="32"> Solar System: Asteroids </option>
                        <option value="33"> Solar System: Comets </option>
                        <option value="34"> Solar System: Moons </option>
                        <option value="35"> Solar System: Ice Sheets </option>
                        <option value="36"> Solar System: Planetary Geology </option>
                        <option value="37"> Solar System: Earth </option>
                        <option value="38"> Solar System: Jupiter </option>
                        <option value="39"> Solar System: Mars </option>
                        <option value="40"> Solar System: Neptune </option>
                        <option value="41"> Solar System: Pluto </option>
                        <option value="42"> Solar System: Saturn </option>
                        <option value="43"> Solar System: Uranus </option>
                        <option value="44"> Solar System: Venus </option>
                        <option value="45"> Solar System: Mercury </option>
                        <option value="88"> Solar System: Comparative planetology </option>
                        <option value="92"> Solar System: Geomorphology </option>
                        <option value="93"> Solar System: Ice </option>
                        <option value="96"> Solar System: Minor planets </option>
                        <option value="98"> Solar System: Volcanology </option>
                        <option value="101"> Solar System: Planetary surfaces </option>
                        <option value="102"> Solar System: Theory &amp; Modeling </option>
                        <option value="104"> Solar System: Meteorites </option>
                        <option value="106"> Solar System: Analysis </option>
                        <option value="109"> Solar System: Technology &amp; Missions </option>
                        <option value="111"> Solar System: Extrasolar Planets </option>
                        <option value="9"> Astrophysics: Stars </option>
                        <option value="10"> Astrophysics: X-ray Binaries &amp; CVs </option>
                        <option value="11"> Astrophysics: Supernovae &amp; SNRs </option>
                        <option value="12"> Astrophysics: Extrasolar planets </option>
                        <option value="13"> Astrophysics: Planetary Disk Formation </option>
                        <option value="14"> Astrophysics: Galaxies </option>
                        <option value="15"> Astrophysics: Active Galactic Nuclei (AGN) </option>
                        <option value="16"> Astrophysics: Galaxy Clusters </option>
                        <option value="17"> Astrophysics: Gamma-ray Bursts </option>
                        <option value="18"> Astrophysics: Cosmology </option>
                        <option value="19"> Astrophysics: Dark Energy &amp; Dark Matter </option>
                        <option value="20"> Astrophysics: Black Holes </option>
                        <option value="21"> Astrophysics: Gravitational Waves </option>
                        <option value="22"> Astrophysics: Interstellar Medium </option>
                        <option value="23"> Astrophysics: Cosmic Rays </option>
                        <option value="24"> Astrophysics: Technology &amp; Missions </option>
                        <option value="89"> Astrophysics: Pulsars </option>
                        <option value="103"> Astrophysics: X-ray Background </option>
                        <option value="105"> Astrophysics: Theory &amp; Modeling </option>
                        <option value="108"> Astrophysics: Analysis </option>
                        <option value="115"> Astrophysics: Gamma-ray Astrophysics </option>
                        <option value="116"> Astrophysics: X-ray Astrophysics </option>
                    </select>
                </div>
            </div>
            <div class="col-md-4">
                <label></label>
                <div class="row">
                    <div class="col-md-6">
                        <button class="btn btn-3d btn-fresh btn-light form-control">Search</button>
                    </div>
                    <div class="col-md-6">
                        <button class="btn btn-3d btn-sunny btn-light form-control">Reset</button>
                    </div>
                </div>
            </div>
        </fieldset>
        <p><b>By Project Title:</b></p>
        <div class="btn-toolbar" style="margin-bottom: 10px">
            <div class="btn-group btn-group-sm">
                <button class="btn btn-default"><a href="">A</a></button>
                <button class="btn btn-default"><a href="">B</a></button>
                <button class="btn btn-default"><a href="">C</a></button>
                <button class="btn btn-default"><a href="">D</a></button>
                <button class="btn btn-default"><a href="">E</a></button>
                <button class="btn btn-default"><a href="">F</a></button>
                <button class="btn btn-default"><a href="">G</a></button>
                <button class="btn btn-default"><a href="">H</a></button>
                <button class="btn btn-default"><a href="">I</a></button>
                <button class="btn btn-default"><a href="">J</a></button>
                <button class="btn btn-default disabled"><a href="">K</a></button>
                <button class="btn btn-default"><a href="">L</a></button>
                <button class="btn btn-default"><a href="">M</a></button>
                <button class="btn btn-default"><a href="">N</a></button>
                <button class="btn btn-default"><a href="">O</a></button>
                <button class="btn btn-default"><a href="">P</a></button>
                <button class="btn btn-default disabled"><a href="">Q</a></button>
                <button class="btn btn-default disabled"><a href="">R</a></button>
                <button class="btn btn-default"><a href="">S</a></button>
                <button class="btn btn-default"><a href="">T</a></button>
                <button class="btn btn-default"><a href="">U</a></button>
                <button class="btn btn-default"><a href="">V</a></button>
                <button class="btn btn-default disabled"><a href="">W</a></button>
                <button class="btn btn-default"><a href="">X</a></button>
                <button class="btn btn-default disabled"><a href="">Y</a></button>
                <button class="btn btn-default disabled"><a href="">Z</a></button>
            </div>
        </div>
        <div class="clearfix">
            <p class="pull-left"><b>Now displaying records 1 to 25 of 55.</b></p>
            <p class="pull-right" style="margin-right: 20px">
                <span style="padding-right: 10px">Results page:</span>
                <a href="">1</a>
                <a href="">2</a>
                <a href="">3</a>
            </p>
        </div>
        <br/>
        <div class="row">
            <div class="col-sm-2 col-xs-8 col-sm-push-0 col-xs-push-2">
                <a href="http://www.nasa.gov/mission_pages/LRO/overview/index.html" style="display:block;padding-bottom:10px;">
                    <img style="width: 100%;" src="https://science.gsfc.nasa.gov/sed/images/mission_thumbs/lro_100.jpg"/>
                </a>
            </div>
            <div class="col-sm-10 col-xs-12">
                <p class="content-section__subtitle">
                    <a href="http://www.nasa.gov/mission_pages/LRO/overview/index.html">Lunar Reconnaissance Orbiter (LRO)</a>
                </p>
                <p>The Lunar Reconnaissance Orbiter (LRO) is an unmanned spacecraft designed to create a comprehensive atlas of the moon's physical features, radiation environment, temperatures, and resources. The mission places special emphasis on the moon's polar regions, where permanently shadowed craters may contain significant amounts of water ice that future human explorers might be able to exploit. LRO launched on June 18, 2009.</p>
            </div>
        </div>
        <div class="row">
            <hr class="col-md-12"/>
            <div class="col-sm-2 col-xs-8 col-sm-push-0 col-xs-push-2">
                <a href="https://science.gsfc.nasa.gov/sed/index.cfm?fuseAction=projects.view&project_id=244" style="display:block;padding-bottom:10px;">
                    <img style="width: 100%;" src="https://science.gsfc.nasa.gov/sed/images/mission_thumbs/LASP_maven_01.jpg"/>
                </a>
            </div>
            <div class="col-sm-10 col-xs-12">
                <p class="content-section__subtitle">
                    <a href="https://science.gsfc.nasa.gov/sed/index.cfm?fuseAction=projects.view&project_id=244">Mars Atmosphere and Volatile Evolution Mission (MAVEN)</a>
                </p>
                <p>The Mars Atmosphere and Volatile Evolution (MAVEN) mission is the first orbiter devoted to understanding the Martian upper atmosphere. Research suggests suggests that Mars once had a thicker atmosphere and was warm enough for liquid water to flow on the surface. MAVEN's instruments are exploring Mars' upper atmosphere, ionosphere, and interactions with the sun and solar wind to determine the role played by loss of atmospheric gas to space in the evolution of the Martian climate through time. Goddard's Planetary Environments Laboratory developed MAVEN's Neutral Gas and Ion Mass Spectrometer and the Planetary Magnetospheres Laboratory developed MAVEN's magnetometer. The MAVEN mission launched November 18, 2013.</p>
                <p><a href="javascript:void(0)" data-toggle="collapse" data-target="#key-staff-dropdown__1"><b>-/+ Key Staff</b></a></p>
                <p class="collapse" id="key-staff-dropdown__1">
                    - Project Scientist: <a href="https://science.gsfc.nasa.gov/sed/index.cfm?fuseAction=people.jumpBio&navOrgCode=690&navTab=nav_about_us&iphonebookid=11822">Joseph M Grebowsky</a>
                </p>
            </div>
        </div>
        <hr class="col-md-12"/>
        <p><a class="content-section__link" href="https://science.gsfc.nasa.gov/sed/index.cfm?fuseAction=projects.alphabetical&fooonavOrgCode=690&navTab=nav_about_us">See Full Alphabetical List</a></p>
    </div>
</div>
<!-- END MAIN SECTION -->