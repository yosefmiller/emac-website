<?php

$klein->respond('/?', function ($req, $res, $service) {
    $service->pageTitle = 'Model Interface | EMAC';
    $service->isMiniHeader = true;
    $service->isHiddenSidebar = true;
    $service->isForm = true;
    $service->customJSFile = "js/calculation.example.js";
    $service->render('examples/pages/atmos-calculation.php');
});

$klein->respond('POST', '/run', function ($req, $res, $service) {
    /* EDIT THESE VARIABLES (keep date and name): */
    $input_fields = ['calc_date', 'calc_name', 'planet_template', 'surface_gravity', 'planet_radius'];
    $input_lists = [];

    /* Get user id */
    $userID = $service->getUserId->__invoke($res);

    /* Generate calculation tracking id */
    $tracking_id = $userID . "_" . substr(uniqid(),-5 );
    $temp_id     = $req->param('tracking_id');

    /* Add form data */
    $form_data = [ "tracking_id" => $tracking_id ];
    foreach ($input_fields as $field) { $form_data[$field] = getFormData($req, $field); }
    foreach ($input_lists as $field)  { $form_data[$field] = getFormData($req, $field, true); }

    /* Execute Python script in background and output to log */
    $command = "python3 -u python/master.py";
    $raw_log_file = "outputs/" . $tracking_id . ".log";

    $escaped_form_data = escapeshellarg(json_encode($form_data));
    $escaped_log_file = escapeshellarg($raw_log_file);
    exec("$command $escaped_form_data > $escaped_log_file 2>&1 &");

    /* Respond */
    $res->json(["status" => "running", "temp_id" => $temp_id, "input" => $form_data]);
});

/* Load common form routes */
include "form.php";