## About this template

A ready-to-use EMAC model implementation template, with real-time validation, multiple concurrent calculations, persistent session/results, immediately accessible logs, and support for any model language (eg. python).  

##### Architecture
- The `dockerfile-compose.yml` runs `Dockerfile` which extends the `phusion/baseimage` (based on `ubuntu`) and installs dependencies such as `php7.4`, `nginx`, `composer`, and `klein`.  
- The `config/nginx.conf` file points all non-existing public files to `index.php`, which handles routing using the Klein router.  
- `php` is used to communicate with the webpage and keep track of users' calculations.  
- `python` (or any language) performs calculations with the form data and outputs a pre-specified file when completed.  
- `javascript/jquery` is used for front-end validation and dynamic form controls.  

This template makes use of the _Klein.php_ router. Documentation for Klein is available [on GitHub](https://github.com/klein/klein.php).  
This template also uses a few client-side libraries: [Bootstrap](http://getbootstrap.com/), [jQuery](https://jquery.com/), [Select2](http://select2.github.io/), [Bootstrap Validator](https://github.com/nghuuphuoc/bootstrapvalidator/tree/v0.5.2), [Plotly](https://plot.ly/javascript)

---
## Layout Customization

##### Files
- `pages/partials/master-layout.php`: _Master layout_ -- Every page first goes here. It loads the various layout components.
- `pages/partials/main-header.php`: _Main header_  
- `pages/partials/sub-header.php`: _Sub header_ -- See below for customization.  
- `pages/partials/sub-navigation.php`:  _Sub navigation_ -- Populated using `pages/models/sub-navigation.json`.  
- `pages/partials/sidebar.php`: _Sidebar_  
- `pages/partials/footer.php`: _Footer_  
- `css/layout.css`: Headers, navigation, sidebar, footer, etc.  
- `css/components.css`: 3D buttons, arrowed labels, image captions, and story sections (for articles, blogs)  
- `css/form.css`: Radio chooser, validation, range slider, and loading spinner  
- `css/site.css`: _Site-specific customizations_ -- color theme and background images.  
- `js/main.js`: Search bar, navigation dropdown, sidebar toggle, and Select2 initialization
- `img`: Images and logos. Store site-specific images in a sub-directory, eg. `img/emac`

###### pages/partials/sub-header.php

````html
<div class="container">
    <div class="row">
        <!-- Remember to correct the bootstrap column widths appropriately. -->
        <!-- Default: left. To align right, add class `nasa__sub-name-right` -->
        <div class="nasa__sub-name col-md-3">
            <div><a href="/">Code 690</a></div>
        </div>
        
        <!-- Default: right. To align left, add class `nasa__sub-logo-left` -->
        <div class="nasa__sub-logo col-md-3">
            <img src="img/emac/emac_logo_transparent.png" alt="EMAC" />
        </div>
        
        <!-- Leave this last so the others are displayed in the right order. -->
        <div class="nasa__sub-name col-md-6">
            <div>
                <a href="/">
                    <!-- Add a `div` for a subtitle, like so: -->
                    <div>Sciences and Exploration Directorate</div>
                    Solar System Exploration Division
                </a>
            </div>
        </div>
    </div>
</div>
````

---
## Configure Routing
###### index.php
````php
<?php

/* Initialize Klein router (from Composer): */
require "../vendor/autoload.php";
$klein = new \Klein\Klein();

/* Configure Routing: */
include "routes/common.php";
include "routes/app.example.php";   // Change this as appropriate to reflect project's route file
include "routes/examples.php";      // Remove for production
include "routes/docs.php";          // Remove for production
include "routes/errors.php";

/* Execute all changes: */
$klein->dispatch();

````

###### routes/app.php
````php
<?php

$klein->respond('/?', function ($req, $res, $service) {
    $service->pageTitle = 'A clever page title goes here | EMAC';   // (required) page/tab title
    $service->isMiniHeader = true;                      // (optional) hides main-header and sub-navigation, shrinks sub-header
    $service->isHiddenSidebar = true;                   // (optional) hides sidebar
    $service->isHiddenMainNav = true;                   // (optional) hides main-navigation
    $service->isHiddenSubNav = true;                    // (optional) hides sub-navigation
    $service->isForm = true;                            // (optional) includes necessary scripts and styles for calculation forms
    $service->customJSFile = "js/some-calculation.js";  // (optional) custom javascript file, useful for form pages
    $service->someVariable = json_decode(file_get_contents("pages/models/some-variable.json"), true);
                                                        // (optional) passes custom data to the page (see examples for usage)
    $service->sidebarPath = 'examples/partials/sidebar.php';      // (optional) custom sidebar
    $service->subHeaderPath = 'examples/partials/sub-header.php'; // (optional) custom sub-header
    $service->footerPath = 'examples/partials/footer.php';        // (optional) custom footer
    $service->render('pages/some-page.php');            // (required) renders view inside layout
});
````

###### pages/some-page.php
````php
<div class="col-md-12">
    <h1>Welcome to some new page! Next up: create a form to run some mind-blowing calculations!</h1>
</div>
````

---
## Create a form

Make sure to set the `isForm` option to true.  

###### pages/some-calculation.php
````html
<!-- Title -->
<h1 class="page-header">New Calculation</h1>

<!-- Calculation Form -->
<form enctype="multipart/form-data" class="form-horizontal" action="" method="post" id="calculation-form">
    <!-- At the beginning of the form: -->
    <div class="row">
        <label class="col-md-2 control-label" for="calc_name">Name</label>
        <div class="col-md-4 form-group">
            <input type="text" class="form-control" id="calc_name" name="calc_name" placeholder="My new calculation (optional)">
            <label for="calc_name" class="help-block">Provide a name for this calculation</label>
        </div>
    </div>
    
    <!-- ADD ALL INPUTS HERE -->
    
    <!-- At the end of the form: -->
    <div class="row">
        <div class="col-md-offset-2 col-md-10">
            <input type="hidden" name="calc_date" id="calc_date" value="" />
            <input type="hidden" name="tracking_id" id="tracking_id" value="0" />
            <button type="submit" class="btn btn-fresh">Submit</button>
        </div>
    </div>
</form>

<!-- Result list -->
<div id="calculation-list"></div>
<div id="calculation-logs"></div>
<div id="calculation-result">
    <div class="col-md-12">
        <h4>Plots</h4>
        <div class="col-md-6"><div id="vmrPlot" style="width: 100%;"></div></div>
        <div class="col-md-6"><div id="tpPlot" style="width: 100%;"></div></div>
    </div>
    <!-- Output/Input tables are inserted here -->
</div>
````

---
## Validate the form
Set the rules! It's all about creating some simple json. Validators are executed in order.  
This template uses an old open-source version of the [Form Validation](http://formvalidation.io) plugin called [Bootstrap Validator](https://github.com/nghuuphuoc/bootstrapvalidator).  
Here's an example configuration. Many more validators are available.  
You can even code a custom validator. See the Pandexo form for an example, such as checking files for valid columns of numbers.  

_Important: This does not replace server-side validation! You must handle invalid and potentially dangerous user-data within Python!_

###### assets/js/some-calculation.js
````js
$.FORM_PREFIX = "/";            // directory in which form is being served. default: current directory
$.FORM_CHECK_INTERVAL = 3000;   // how often to check for calculation results in milliseconds. default: 3000
$.FORM_INPUT_ORDER = ["some_input_name", "planet_mass", "planet_radius"];   // order to display input values. default: alphabetically
$.FORM_OUTPUT_ORDER = ["some_output_name", "Mission", "Max Period"];        // order to display output values. default: alphabetically
$.validationConfigFields = {
    some_input_name: {
        validators: {
            notEmpty: {
                message: 'Surface gravity is required'
            },
            numeric: {
                message: 'Surface gravity must be a number'
            },
            between: {
                min: 0.75,
                max: 1.5,
                message: 'Surface gravity must be between 0.75g and 1.5g'
            }
        }
    }
};

/* Display the plots and data according to tool's specifications, using Plotly.js */
$.plotResult = function (response) {
    var vmrPlot = $('#vmrPlot')[0];
    var vmrFile = "outputs/" + response.vmr_file;
    var vmrLayout = {
        xaxis: { type: "log", range:[-11,0.5],      title: "Abundance",      titlefont:{size:12}},
        yaxis: { type: "log", autorange:"reversed", title: "Pressure [bar]", titlefont:{size:12}},
        legend: { xanchor: "left", yanchor:"top", y:1.0, x:0.0},
        margin: { l:50, r:0, b:30, t:0, pad:0 }
    };
    var vmrY = "Press";
    var vmrX = ["H2O", "CH4", "C2H6", "CO2", "O2", "O3", "CO", "H2CO", "HNO3", "NO2", "SO2", "OCS"];
    
    /**
     *  Plot data using Plotly from a datafile:
     *  - plot:           (dom element) div to host plot
     *  - layout:         (object) passed directly to plotly for layout/title/axis/legend/shapes
     *  - outputFileUrl:  (url) plaintext file in which each column (delineated by white-space) contains the column-name followed by the values
     *  - yNames:         (string or array) name of column containing y values
     *  - xNames:         (array) name of column containing x values
     *  - customData:     (object, optional) added to each data entry
     *  - customDataList: (array of objects, optional) added to each corresponding data entry
     **/
    $.plotData(vmrPlot, vmrLayout, vmrFile, vmrY, vmrX);
};

/* Must call these to get started */
$.initValidation();
$.initCalculationList();
````

---
## Running calculations

#### How it works:  
1. Submits the form using `POST` to `/run`.<br/>
2. The user is tracked with session tokens (until browser is closed). This identifier can be accessed in PHP by calling `$userID = $service->getUserId->__invoke($res);` from within a route.
3. PHP processes the form:<br/>Validates the form to prevent hacks.<br/>Executes the given Python script in the background, and passes it the form data as JSON.<br/>Responds to Javascript with JSON (status 'running'), as discussed soon.
4. Javascript periodically pings PHP (at `/check/<tracking_id>`), checking for the creation of a response file.
5. Python outputs a response file with calculation results.
6. Javascript marks the calculation as complete.

#### Handle form submission:
Copy the remainder of `routes/app.example.php` to the following file. and then change lines with form inputs:  

###### routes/app.php
````php
$klein->respond('POST', '/run', function ($req, $res, $service) {
    /* EDIT THESE VARIABLES (keep date and name): */
    $input_fields = ['calc_date', 'calc_name', 'planet_template', 'surface_gravity', 'planet_radius'];
    $input_lists = ['missions'];

    // COPY REMAINDER OF FILE...
````

#### Time to write Python code!
Modify the `python/master.py` file as indicated. The following example demonstrates how to include output values, files, and log info:   

###### python/master.py
````python
########## CALCULATION BEGIN ##########

import example_calculation
example_calculation.calculate()

########## CALCULATION END ##########
````

###### python/example_calculation.py
````python
from helpers import form_data, response

def calculate ():
    # Store data into individual variables
    planet_template = form_data.get("planet_template", type="str")
    surface_gravity = form_data.get("surface_gravity", type="float")
    planet_radius   = form_data.get("planet_radius",   type="float")

    # Run calculations...
    import time
    time.sleep(3)
    print("``This is some verbose information.")
    response.add("Planet Radius", planet_radius)

    # Output some plot results
    output_file_name = "outputs/" + form_data.id() + "_someplot.pt"
    with open(output_file_name, "w") as f:
        f.write("frequencies period power \n")
        for index in pData:
            f.write(str(p.frequency[index]) + " " +
                    str(p.period[index]) + " " +
                    str(p.power[index]) + "\n")
        f.close()
    response.add("sample_file", output_file_name)
````

#### Plot results:
Customize the `plotResult` Javascript function to display the plots according to your specifications (see above).  

#### Make discoveries...